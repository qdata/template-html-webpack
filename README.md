# Template HTML - WEBPACK

Template base para proyectos creados unicamnete con html css y js

## Requerimientos

✅   Tener instalado webpack y webpack-cli

✅   Tener instalado Node  en su version es LTS y npm

## Instalación

⚙️ Correr en consola en la raiz del proyecto npm i , para instalar las dependencias


## Inicializar Webpack

```bash
npm run dev
```

## Recomendaciones

❗️ Clonar del repo , sacar una copia y trabajar sobre la copia.

❗️ No modificar , ni eliminar ninguna configuración o archivo del template.

❗️ Si ve alguna mejora que se le pueda hacer a este template, no hacer el cambio sobre el repo y subirlo, informarlo primero para validarlo.


## License
[ISC](https://choosealicense.com/licenses/isc/)